// Применение литералов, определяемых пользователем. для морских вычислений. 
// Заимстовано из слайдов лекций Алексея Валерьевича Мартынова.
// Лекция №4 (Функции), слайды 20-24

// Английские судоходные термины
// https://nevatec.spb.ru/anglijskie-sudoxodnye-terminy-i-sokrashheniya/


// NAUTICAL_MILE
//http://programming-code.com/C-Plus-Plus-Program/Overview/Structure-of-Cpp-Programs/cpp-program-calculating-the-distance-between-2-latitudes-and-longitudes
/*
C++ Program to receive values of latitude and longitude in degress, of two places on earth and outputs the distance between them in nautical miles. The formula for distance in nautical is : 
D=3963 cos-1 (sinL1sinL2+cosL1cosL2*cos(G2-G1))

#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    float L1, L2, G1, G2, D;
    
    cout << "Enter Two Values of Lattitude : ";
    cin >> L1 >> L2;
    
    cout << "\nEnter Two Values of Longitude : ";
    cin >> G1 >> G2;
    
    D = 3963 * acos(sin(L1) * sin(L2) + cos(L1) * cos(L2) * cos(G2 - G1));
    
    cout << "\n\nDistance is : " << D;
    
    return 0;
}
*/

#include <iostream>
#include <chrono>

#define NAUTICAL_MILE 1852 // meters, https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=2ahUKEwi8lJvZ_4bhAhVtyqYKHQWjC0IQFjABegQICRAB&url=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%25D0%259C%25D0%25BE%25D1%2580%25D1%2581%25D0%25BA%25D0%25B0%25D1%258F_%25D0%25BC%25D0%25B8%25D0%25BB%25D1%258F&usg=AOvVaw1WpOOm2lBZJJQ0sAkwCMg5
#define KILO 					1000

// Слайд 20 - Мотивация
	struct distance_t {
		double meters;
	};

	struct speed_t {
		double ms;
	};


	distance_t operator +(const distance_t & lhs,
			const distance_t & rhs) {
	/*	
				double sum = lhs.meters + rhs.meters;
		  	distance_t r;
		  	r.meters = sum;
		  	return r;
	*/
	/*
		distance_t r{ lhs.meters + rhs.meters };
		return r;	
	*/
	// Замечание: такой возврат значение поможет компилятору оптимизировать возвращаемое значение
	// (RVO - Return Value Optimization - построить возвращаемый объект прямо в памяти вызывающего клиента,
	// избавившись от копирования (Майерс, правило 20)		  
		std::cout << __LINE__ << ": " << __FUNCTION__ << std::endl;	
		return distance_t { lhs.meters + rhs.meters };
	}

  distance_t operator +(const distance_t & lhs,
			const double & meters) {
	/*	
				double sum = lhs.meters + rhs.meters;
		  	distance_t r;
		  	r.meters = sum;
		  	return r;
	*/
	/*
		distance_t r{ lhs.meters + rhs.meters };
		return r;	
	*/
	// Замечание: такой возврат значение поможет компилятору оптимизировать возвращаемое значение
	// (RVO - Return Value Optimization - построить возвращаемый объект прямо в памяти вызывающего клиента,
	// избавившись от копирования (Майерс, правило 20)		  
		std::cout << __LINE__ << ": " << __FUNCTION__ << std::endl;	
		return distance_t { lhs.meters + meters };
	}

	std::ostream & operator << (std::ostream & out, const distance_t & d) {
		out << d.meters << " meters";
		return out;
	}

	distance_t operator -(const distance_t & lhs,
			const distance_t & rhs);

  speed_t operator +(const speed_t & lhs, const speed_t & rhs);
	speed_t operator -(const speed_t & lhs, const speed_t & rhs);

	speed_t operator /(const distance_t & d,
			const std::chrono::seconds & t) {
		return speed_t { d.meters / std::chrono::duration_cast<std::chrono::seconds>(t).count() };
	}
	std::chrono::seconds operator /(const distance_t & d,
			const speed_t & s) {
		unsigned long int t = d.meters / s.ms; 
		return { std::chrono::duration_cast<std::chrono::seconds>(std::chrono::seconds(t)) };
	}
	distance_t operator *(const speed_t & s,
			const std::chrono::seconds & t);
	distance_t operator *(const std::chrono::seconds & t,
			const speed_t & s);
	
	std::ostream & operator << (std::ostream & out, const speed_t & s) {
		out << s.ms << " meters/sec";
		return out;
	}

	// Слайд 21 Правила 
	/*
	1. extern "C" запрещен
	2. Параметры:
	∙ const char *
	∙ unsigned long long int
	∙ long double
	∙ char
	∙ wchar_t
	∙ char16_t
	∙ char32_t
	∙ const char *, std::size_t
	∙ const wchar_t *, std::size_t ∙ const char16_t *, std::size_t ∙ const char32_t *, std::size_t
	3. Преобразование unsigned long long int → long double не применяется
	4. C++ 11 type operator ”” suffix(...) C++ 14 type operator ””suffix(...)
	5. оператор может быть реализован в виде шаблона с переменным числом аргументов
	*/

 // Слайд 23 Реализация
distance_t operator "" _m(long double meters)
{
	return distance_t { static_cast< double >(meters)};
}

distance_t operator "" _km(long double km)
{
	return distance_t{ static_cast<double> (km) * KILO };
}

distance_t operator "" _nm(long double miles)
{
	return distance_t { static_cast< double > (miles) * NAUTICAL_MILE };
}

speed_t operator "" _ms(long double ms)
{
	return speed_t{ static_cast< double >(ms) };
}

speed_t operator "" _kmh(long double kmh)
{
	return speed_t{ static_cast <double> (kmh) / 3.6 };
}

// Knot — Узел, единица скорости самолета или судна, равная 1.15 мили/час или 1.85 км/ч
speed_t operator "" _knot(long double knots)
{
	return speed_t{ static_cast< double >(knots) * NAUTICAL_MILE / 3600 };
}



int main()
{

// Слайд 21 Мотивация

	const distance_t leg1 = { 25.89 * NAUTICAL_MILE };
	const distance_t leg2 = { 1.9 * NAUTICAL_MILE };
	const std::chrono::seconds time = 
		std::chrono::duration_cast<std::chrono::seconds>
			(std::chrono::hours(2));

	const speed_t average_speed = (leg1 + leg2) / time;	 	

	std::cout << __LINE__ << ": " << average_speed << std::endl;


// Слайд 24 Использование

	using namespace std::chrono_literals; // это для того, чтобы можно было написать '2h'

// используем свои пользовательские литералы!!!

	const distance_t route = 25.89_nm + 1.9_nm;  // leg1 + leg2

	const std::chrono::seconds elapsed_time = 2h; // реальное время в пути
	const speed_t contract_speed = 16.0_knot; // скорость по контракту (в узлах)

	const speed_t route_speed = route / elapsed_time;
	const std::chrono::seconds eta = route / contract_speed; // Estimated Time of Arrival

	std::cout << __LINE__ << ": " << route_speed << std::endl;
	// расчётное время в пути в секундах (если бы 16 узлов)
	std::cout << __LINE__ << ": " << eta.count() << " sec" << std::endl; 

	double eta_double = eta.count();
	double secs_per_hour_double = 3600.0;
	double eta_hours = eta_double / secs_per_hour_double;
	// расчетное время в пути в часах (если бы 16 узлов)
  std::cout << __LINE__ << ": " << eta_hours << " hours" << std::endl; 

// посчитаем реальную скорость в узлах
//
//	7.14821 (meters/sec) * 3.6 = 25.733556 (km/h)
//	25.733556 / (NAUTICAL_MILE/1000) = 13.895 (knot)
//  Реальная скорость чуть меньше 14 узлов, поскольку были в пути 2 часа 
//	   
//  Это посчитано на калькуляторе - для расчетов в единицах этой программы нужно дополнить операторы.
 
  distance_t sum = distance_t{.meters = 10} + distance_t{5} + 10.0;

  std::cout << __LINE__ << ": " << sum << std::endl;

	return 0;
}